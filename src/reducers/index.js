import { combineReducers } from "redux";
const chat = (
  state = { preloader: true, editModal: false, messages: [] },
  action
) => {
  switch (action.type) {
    case "setMessages":
      return {
        ...state,
        messages: action.payload.messages,
      };
    case "sendMessage":
      const newMessages = state.messages.concat(action.payload.message);
      return {
        ...state,
        messages: newMessages,
      };
    case "hidePreloader":
      return {
        ...state,
        fetched: true,
        preloader: false,
      };
    case "startEdit":
      const text = state.messages.find(
        (msg) => msg.id === action.payload.editedMessage
      )?.text;
      return {
        ...state,
        editModal: true,
        editing: action.payload.editing,
        editedMessage: action.payload.editedMessage,
        editedMessageText: text,
      };

    case "editMessage":
      return {
        ...state,
        messages: state.messages.map((message) => {
          if (message.id === state.editedMessage) {
            message = action.payload.message;
          }
          return message;
        }),
        editing: action.payload.editing,
        editedMessage: null,
        editModal: false,
        editedMessageText: "",
      };
    case "setEditedText":
      return {
        ...state,
        editedMessageText: action.payload.editedMessageText,
      };
    case "setUser":
      return {
        ...state,
        user: action.payload.user,
      };
    default:
      return state;
  }
};

const rootReducer = combineReducers({
  chat,
});
export default rootReducer;
