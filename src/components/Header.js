import React from "react";
import "./css/Header.css";
import moment from "moment";
import { useSelector } from "react-redux";
function Header(props) {
  const messages = useSelector((state) => state.chat.messages);
  return (
    <div className="header">
      <div className="header-title">Best chat ever</div>
      <div className="header-users-cofunt">{getUsersNum(messages)}</div>
      <div className="header-messages-count">{messages.length}</div>
      <div className="header-last-message-date">
        {getFullDate(messages[messages.length - 1]?.createdAt)}
      </div>
    </div>
  );
}

const getFullDate = (data) => {
  const format = "DD.MM.YYYY HH:mm";
  var date = new Date(data);
  let dateTime = moment(date).format(format);
  return dateTime;
};

const getUsersNum = (messages) => {
  const result = [];
  messages.forEach((item) => {
    if (!result.includes(item.user)) result.push(item.user);
  });
  return result.length;
};

export default Header;
