import React from "react";
import "./css/MessageInput.css";
import { sendMessage } from "../actions/messages";
import { useDispatch, useSelector } from "react-redux";
import { v4 as uuidv4 } from "uuid";

function MessageInput(props) {
  const user = useSelector((state) => state.chat.user);
  const dispatch = useDispatch();
  const sendMessageLocal = () => {
    const input = document.querySelector(".message-input-text");
    const id = uuidv4();
    const messageText = input.value;
    const TDate = new Date();
    const message = {
      id: id,
      text: messageText,
      createdAt: TDate,
      userId: user.id,
      user: user.login,
      avatar: "https://www.aceshowbiz.com/images/photo/tom_pelphrey.jpg",
    };

    dispatch(sendMessage(message));
    fetch("http://localhost:3001/message", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(message),
    });
    input.value = "";
  };
  return (
    <div className="message-input">
      <input type="text" className="message-input-text" autoFocus></input>
      <button className="message-input-button" onClick={sendMessageLocal}>
        Send
      </button>
    </div>
  );
}
export default MessageInput;
