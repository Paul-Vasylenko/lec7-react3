import React from "react";
import "./css/MessageList.css";
import Message from "./Message";
import OwnMessage from "./OwnMessage";
import Devider from "./Devider";
import { v4 as uuidv4 } from "uuid";
import { useSelector } from "react-redux";
const isDateEqual = (date1, date2) => {
  return (
    new Date(date1).getFullYear() === new Date(date2).getFullYear() &&
    new Date(date1).getDate() === new Date(date2).getDate() &&
    new Date(date1).getMonth() === new Date(date2).getMonth()
  );
};

function MessageList(props) {
  const user = useSelector((state) => state.chat.user);
  const messages = useSelector((state) => state.chat.messages);
  return (
    <div className="message-list">
      {messages.map((message, index, array) => {
        const toBeReturned = [];
        if (
          index === 0 ||
          !isDateEqual(message.createdAt, array[index - 1].createdAt)
        ) {
          toBeReturned.push(
            <Devider time={message.createdAt} key={uuidv4()} />
          );
        }
        message.userId == user.id
          ? toBeReturned.push(
              <OwnMessage message={message} key={uuidv4()} id={message.id} />
            )
          : toBeReturned.push(
              <Message data={{ message }} key={uuidv4()} id={message.id} />
            );
        return toBeReturned;
      })}
    </div>
  );
}
export default MessageList;
