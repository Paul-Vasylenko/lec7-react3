import React, { useEffect, useState } from "react";
import { Link, useHistory } from "react-router-dom";
import Logo from "./Logo";
import UserItem from "./UserItem";
import "./css/UserList.css";
import { useSelector } from "react-redux";
function UserList(props) {
  const history = useHistory();
  const user = useSelector((state) => state.chat.user);
  if (!user || !user.login) {
    history.push("/login");
    console.log("user");
  } else if (user.role !== "admin") {
    history.push("/chat");
  }
  const [users, setUsers] = useState([]);
  useEffect(() => {
    let mounted = true;
    fetch("http://localhost:3001/users")
      .then((res) => res.json())
      .then((json) => {
        if (mounted) {
          setUsers(json);
        }
      });
    return () => (mounted = false);
  }, []);

  const addUserHandler = () => {
    props.setEditingUser(false);
    history.push("/userForm");
  };
  return (
    <div>
      <Logo />
      <header>
        <ul>
          <li
            onClick={() =>
              history.push({
                pathname: "/chat",
                state: {
                  user,
                },
              })
            }
          >
            to chat
          </li>
        </ul>
      </header>
      <button className="addBtn" onClick={addUserHandler}>
        Add user
      </button>
      <div className="user-container">
        <div className="user-block info">
          <div className="user-login item">LOGIN</div>
          <div className="user-password item">PASSWORD</div>
          <div className="user-role item">ROLE</div>
          <button>DeleteBtn</button>
          <button>EditBtn</button>
        </div>
        {users.map((user) => (
          <UserItem
            key={user.id}
            user={user}
            setEditingUser={props.setEditingUser}
            setEditedUser={props.setEditedUser}
          />
        ))}
      </div>
    </div>
  );
}
export default UserList;
