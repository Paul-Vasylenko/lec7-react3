import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { setUserStore } from "../actions/messages";
import "./css/Login.css";
function Login(props) {
  const [shownIncorrect, setShownIncorrect] = useState(false);
  const dispatch = useDispatch();
  const handleSubmit = async (e) => {
    e.preventDefault();
    const login = e.target.elements.login.value;
    const password = e.target.elements.password.value;
    const token = await loginUser({
      login,
      password,
    });
    dispatch(setUserStore(token));
    if (!token) {
      setShownIncorrect(true);
    }
  };

  return (
    <div className="login">
      {shownIncorrect ? <h3>Enter correct credentials</h3> : null}
      <form onSubmit={handleSubmit}>
        <input type="text" placeholder="Enter login..." name="login"></input>
        <input
          type="password"
          placeholder="Enter password..."
          name="password"
        ></input>
        <input type="submit" value="Login"></input>
      </form>
    </div>
  );
}

async function loginUser(credentials) {
  return fetch(`http://localhost:3001/users`, {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
    },
  })
    .then((data) => data.json())
    .then((json) => {
      const users = json.filter((user) => {
        if (
          user.login == credentials.login &&
          user.password == credentials.password
        ) {
          return user;
        }
      });
      return users;
    })
    .then((users) => users[0]);
}

export default Login;
