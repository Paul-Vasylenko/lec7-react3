import React from "react";
import "./css/MessageList.css";
import "./css/Message.css";
import "./css/OwnMessage.css";
import moment from "moment";
import { useDispatch, useSelector } from "react-redux";
import { setMessages, startEditingMessage } from "../actions/messages";
import { useHistory } from "react-router-dom";

function OwnMessage(props) {
  const history = useHistory();
  const messagesProps = useSelector((state) => state.chat.messages);
  const dispatch = useDispatch();
  const onDelete = (e) => {
    const id = e.target.parentElement.id;
    const messages = messagesProps.filter((item) => {
      if (item.id !== id) {
        return item;
      }
    });
    dispatch(setMessages(messages));
    fetch("http://localhost:3001/message/" + id, {
      method: "DELETE",
    });
  };
  const editMessage = (e) => {
    const messageID = e.target.parentElement.id;
    dispatch(startEditingMessage(messageID));
    history.push({
      pathname: "/editmsg",
      state: {
        editedMessageId: messageID,
      },
    });
  };
  let message = props.message;
  return (
    <div className="message own-message" id={props.id}>
      <p className="message-text">{message.text}</p>
      <p className="message-time">{getTimeHHMM(message.createdAt)}</p>
      <i className="fas fa-pencil-alt message-edit" onClick={editMessage}></i>
      <i className="fas fa-trash-alt message-delete" onClick={onDelete}></i>
    </div>
  );
}

export const getTimeHHMM = (data) => {
  const format = "HH:mm";
  var date = new Date(data);
  let dateTime = moment(date).format(format);
  return dateTime;
};
export default OwnMessage;
