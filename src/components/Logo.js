import React from "react";
import "./css/Logo.css";
import { Link } from "react-router-dom";
function Logo(props) {
  return (
    <div className="logo">
      <img className="logo-img" src="/images/logo.jpg" alt="logo" />
      <Link to="/">
        <span className="logo-text">WebChat</span>
      </Link>
    </div>
  );
}

export default Logo;
