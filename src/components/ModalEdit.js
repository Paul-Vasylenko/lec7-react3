import React from "react";
import { useDispatch, useSelector } from "react-redux";
import "./css/modaledit.css";
import { v4 as uuidv4 } from "uuid";
import { setEditedText, editMessage } from "../actions/messages";
import { useHistory, Link } from "react-router-dom";
function ModalEdit(props) {
  const history = useHistory();
  const user = useSelector((state) => state.chat.user);
  if (!user || !user.login) {
    history.push("/login");
  }
  const editing = useSelector((state) => state.chat.editing);
  const editedMessage = useSelector((state) => state.chat.editedMessage);
  const messages = useSelector((state) => state.chat.messages);
  const editedMessageText = useSelector(
    (state) => state.chat.editedMessageText
  );
  const dispatch = useDispatch();

  const editMessageClick = () => {
    const inputArea = document.querySelector(".edit-message-input");
    const input = inputArea.value;
    const editedMessageObj = Object.assign(
      {
        id: editedMessage,
        userId: user.id,
        user: user.login,
        avatar: "https://www.aceshowbiz.com/images/photo/tom_pelphrey.jpg",
      },
      {
        text: input,
        createdAt: new Date(),
      }
    );
    dispatch(editMessage(editedMessageObj));
    fetch("http://localhost:3001/message/" + editedMessage, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(editedMessageObj),
    });
  };
  const closeModal = () => {
    const id = editedMessage;
    const message = messages.find((msg) => msg.id === id);
    dispatch(editMessage(message));
  };

  const _handlechange = (e) => {
    dispatch(setEditedText(e.target.value));
  };
  const classListBlock = editing
    ? "edit-message-modal modal-shown"
    : "edit-message-modal";

  return (
    <div>
      <div className={classListBlock}>
        <label>Enter new text-message:</label>
        <div key={uuidv4()}></div>
        <input
          type="text"
          className="edit-message-input"
          value={editedMessageText || ""}
          onChange={_handlechange}
        ></input>
        <button
          className="edit-message-button edit-btn"
          onClick={editMessageClick}
        >
          Change
        </button>
        <button className="edit-message-close edit-btn" onClick={closeModal}>
          Close
        </button>
      </div>
      <Link to="/chat">
        <h2>To chat</h2>
      </Link>
    </div>
  );
}

export default ModalEdit;
