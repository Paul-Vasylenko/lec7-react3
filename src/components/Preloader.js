import React from "react";
import "./css/Preloader.css";

function Preloader(props) {
  return (
    <div className="preloader">
      <div className="spinner"></div>
    </div>
  );
}

export default Preloader;
