import React from "react";
import "./css/Devider.css";
import moment from "moment";
function Devider(props) {
  return (
    <div className="messages-divider">
      <span>
        {moment(new Date(props.time)).calendar(null, {
          lastDay: "[Yesterday]",
          sameDay: "[Today]",
          sameElse: `dddd, DD MMMM`,
        })}
      </span>
    </div>
  );
}
export default Devider;
