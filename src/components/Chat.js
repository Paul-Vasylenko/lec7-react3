import React, { useEffect, useState } from "react";
import Preloader from "./Preloader";
import { Link, useLocation } from "react-router-dom";
import Logo from "./Logo";
import MessageInput from "./MessageInput";
import MessageList from "./MessageList";
import Header from "./Header";
import ModalEdit from "./ModalEdit";
import { useHistory } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { setMessages, startEditingMessage } from "../actions/messages";
function Chat(props) {
  const user = useSelector((state) => state.chat.user);
  const [isFetched, setFetched] = useState(false);
  const dispatch = useDispatch();
  const history = useHistory();
  if (!user || !user.login) {
    history.push("/login");
    console.log("user");
  }
  const messages = useSelector((state) => state.chat.messages);
  useEffect(() => {
    let mounted = true;
    fetch("http://localhost:3001/messages")
      .then((res) => res.json())
      .then((json) => {
        json.sort((message1, message2) => {
          if (message1.createdAt < message2.createdAt) return -1;
          if (message1.createdAt === message2.createdAt) return 0;
          if (message1.createdAt > message2.createdAt) return 1;
        });
        if (mounted) {
          dispatch(setMessages(json));
          setFetched(true);
          /*fetch initial messages */
          // fetch("http://localhost:3001/messages", {
          //   method: "POST",
          //   headers: {
          //     "Content-Type": "application/json",
          //   },
          //   body: JSON.stringify(json),
          // });
        }
      });
    return () => (mounted = false);
  }, []);
  useEffect(() => {
    window.addEventListener("keyup", (e) => {
      if (e.key === "ArrowUp") {
        const currentMessages = messages;
        if (currentMessages) {
          const lastMessage = currentMessages[currentMessages.length - 1];
          if (lastMessage && lastMessage?.userId === user.id) {
            dispatch(startEditingMessage(lastMessage.id));
            history.push("/editmsg");
          }
        }
      }
    });
  }, [messages, dispatch]);
  if (!isFetched) {
    return <Preloader />;
  } else {
    return (
      <div className="chat">
        <Logo />
        {user?.role === "admin" ? (
          <Link to="/userList">
            <h3>to user list</h3>
          </Link>
        ) : null}
        <Header />
        <MessageList />
        <MessageInput />
        <ModalEdit />
      </div>
    );
  }
}

export default Chat;
