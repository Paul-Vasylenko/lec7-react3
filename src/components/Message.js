import React from "react";
import "./css/MessageList.css";
import moment from "moment";
import "./css/Message.css";

function Message(props) {
  const likeMessage = (e) => {
    e.target.classList.toggle("message-like");
    e.target.classList.toggle("message-liked");
  };
  let message = props.data.message;
  return (
    <div className="message">
      <p className="message-text">{message.text}</p>
      <p className="message-time">{getTimeHHMM(message.createdAt)}</p>
      <p className="message-user-name">{message.user}</p>
      <img className="message-user-avatar" alt="avatar" src={message.avatar} />
      <i className="fas fa-heart message-like" onClick={likeMessage}></i>
    </div>
  );
}

const getTimeHHMM = (data) => {
  const format = "HH:mm";
  var date = new Date(data);
  let dateTime = moment(date).format(format);
  return dateTime;
};
export default Message;
