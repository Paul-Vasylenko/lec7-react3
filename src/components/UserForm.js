import React, { useState } from "react";
import { useSelector } from "react-redux";
import { useHistory, useLocation } from "react-router-dom";
import { Link } from "react-router-dom";
function UserForm(props) {
  const location = useLocation();
  const history = useHistory();
  const user = useSelector((state) => state.chat.user);
  if (!user || !user.login) {
    history.push("/login");
  } else if (user.role !== "admin") {
    history.push("/chat");
  }
  const createHandler = async (e) => {
    e.preventDefault();
    const data = {
      login: e.target.elements.login.value,
      password: e.target.elements.password.value,
      role: e.target.elements.role.value,
    };
    fetch("http://localhost:3001/users", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(data),
    });
  };
  const editHandler = async (e) => {
    e.preventDefault();
    const data = {
      login: e.target.elements.login.value,
      password: e.target.elements.password.value,
      role: e.target.elements.role.value,
    };
    fetch("http://localhost:3001/users/" + props.editedUser, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(data),
    });
  };
  if (!location.state) {
    location.state = {
      user: {
        login: "",
        password: "",
        role: "",
      },
    };
  }
  const [login, setLogin] = useState(location.state?.user.login);
  const [password, setPassword] = useState(location?.state.user.password);
  const [role, setRole] = useState(location?.state.user.role);
  const _changeHandlerLogin = (e) => {
    setLogin(e.target.value);
  };
  const _changeHandlerPassword = (e) => {
    setPassword(e.target.value);
  };
  const _changeHandlerRole = (e) => {
    setRole(e.target.value);
  };
  return (
    <div className="form-container">
      {console.log(props)}
      <form onSubmit={props.editing ? editHandler : createHandler}>
        <input
          type="text"
          placeholder="Login"
          name="login"
          onChange={_changeHandlerLogin}
          value={login}
        ></input>
        <input
          type="text"
          placeholder="Password"
          name="password"
          onChange={_changeHandlerPassword}
          value={password}
        ></input>
        <input
          type="text"
          placeholder="Role"
          name="role"
          value={role}
          onChange={_changeHandlerRole}
        ></input>
        <input type="submit" value={props.editing ? "Edit" : "Create"}></input>
      </form>
      <Link to="/userList">
        <h2>To user list</h2>
      </Link>
    </div>
  );
}

export default UserForm;
