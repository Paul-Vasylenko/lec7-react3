import React, { useState } from "react";
import { useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import "./css/UserItem.css";
function UserItem(props) {
  const user = useSelector((state) => state.chat.user);
  const deleteHandler = async () => {
    await fetch("http://localhost:3001/users/" + props.user.id, {
      method: "DELETE",
    });
    setIsShown(false);
  };
  const history = useHistory();
  const editHandler = () => {
    props.setEditingUser(true);
    props.setEditedUser(props.user.id);
    console.log("userForm");
    history.push({ pathname: "/userForm", state: { user: props.user } });
  };
  const [isShown, setIsShown] = useState(true);
  if (!isShown) {
    return null;
  }
  if (props.user.id === user.id) return null;
  return (
    <div className="user-block">
      <div className="user-login item">{props.user.login}</div>
      <div className="user-password item">{props.user.password}</div>
      <div className="user-role item">{props.user.role}</div>
      <button onClick={deleteHandler}>Delete</button>
      <button onClick={editHandler}>Edit</button>
    </div>
  );
}

export default UserItem;
