import Chat from "./components/Chat";
import Login from "./components/Login";
import UserList from "./components/UserList";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  useHistory,
} from "react-router-dom";
import UserForm from "./components/UserForm";
import { useEffect, useState } from "react";
import ModalEdit from "./components/ModalEdit";
import { useSelector } from "react-redux";
export const path = window.location.href;
function App() {
  const history = useHistory();
  const [editingUser, setEditingUser] = useState(false);
  const [editedUser, setEditedUser] = useState(null);
  const user = useSelector((state) => state.chat.user);
  useEffect(() => {
    if (user && user.login) {
      if (user.role === "user") {
        history.push("/chat");
      } else {
        history.push("/userList");
      }
    }
  }, [user]);
  return (
    <Switch>
      <Route path="/" exact>
        <Link to="/login">
          <h1>Login</h1>
        </Link>
      </Route>
      <Route path="/login">
        <Login />
      </Route>
      <Route
        path="/chat"
        exact
        component={() => (
          <Chat url="https://edikdolynskyi.github.io/react_sources/messages.json" />
        )}
      />
      <Route
        path="/userList"
        exact
        component={() => (
          <UserList
            setEditingUser={setEditingUser}
            setEditedUser={setEditedUser}
          />
        )}
      />
      <Route
        path="/userForm"
        exact
        component={() => (
          <UserForm editing={editingUser} editedUser={editedUser} />
        )}
      />
      <Route path="/editmsg" exact component={() => <ModalEdit />} />
    </Switch>
  );
}

export default App;
