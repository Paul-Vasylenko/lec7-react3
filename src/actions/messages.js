export const setMessages = (messages) => {
  return {
    type: "setMessages",
    payload: {
      messages,
    },
  };
};
export const hidePreloader = () => {
  return {
    type: "hidePreloader",
  };
};
export const sendMessage = (message) => {
  return {
    type: "sendMessage",
    payload: {
      message,
    },
  };
};

export const startEditingMessage = (message) => {
  return {
    type: "startEdit",
    payload: {
      editing: true,
      editedMessage: message,
    },
  };
};
export const editMessage = (message) => {
  return {
    type: "editMessage",
    payload: {
      message,
      editing: false,
    },
  };
};

export const setEditedText = (text) => {
  return {
    type: "setEditedText",
    payload: {
      editedMessageText: text,
    },
  };
};
export const setUserStore = (user) => {
  return {
    type: "setUser",
    payload: {
      user: user,
    },
  };
};
