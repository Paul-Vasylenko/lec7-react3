const express = require("express");
const app = express();
const cors = require("cors");
app.use(express.json());
app.use(cors());
app.use(express.urlencoded({ extended: true }));

const routes = require("./routes/index");
routes(app);

const port = 3001;
app.listen(port, () => {
  console.log("listening server on port ", port);
});
