const { Router } = require("express");
const fs = require("fs");
const router = Router();

router.post("/", (req, res) => {
  fs.readFile("./db.json", (err, json) => {
    let obj = JSON.parse(json);
    req.body.forEach((message) => obj.messages.push(message));
    fs.writeFile("./db.json", JSON.stringify(obj), function (err) {
      if (err) console.log("Error");
    });
    return res.status(200).json(obj.messages);
  });
});
router.get("/", (req, res) => {
  fs.readFile("./db.json", (err, json) => {
    let obj = JSON.parse(json);
    const result = JSON.stringify(obj.messages);
    return res.status(200).send(result);
  });
});
module.exports = router;
