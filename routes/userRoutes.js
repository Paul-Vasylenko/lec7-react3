const { Router } = require("express");
const fs = require("fs");
const router = Router();

router.get("/", (req, res) => {
  fs.readFile("./db.json", (err, json) => {
    let obj = JSON.parse(json);
    console.log(obj);
    return res.status(200).json(obj.users);
  });
});

router.delete("/:id", (req, res) => {
  const { id } = req.params;
  fs.readFile("./db.json", (err, json) => {
    let obj = JSON.parse(json);
    const foundUser = obj.users.find((entry) => entry.id == id);
    obj.users.splice(
      obj.users.indexOf(obj.users.find((user) => user.id === foundUser.id)),
      1
    );
    fs.writeFile("./db.json", JSON.stringify(obj), function (err) {
      if (err) console.log("Error");
    });
    return res.status(200).json(obj.users);
  });
});
router.post("/", (req, res) => {
  fs.readFile("./db.json", (err, json) => {
    let obj = JSON.parse(json);
    const user = {
      id: new Date().getTime(),
      ...req.body,
    };
    obj.users.push(user);
    fs.writeFile("./db.json", JSON.stringify(obj), function (err) {
      if (err) console.log("Error");
    });
    return res.status(200).json(obj.users);
  });
});

router.put("/:id", (req, res) => {
  const { id } = req.params;
  fs.readFile("./db.json", (err, json) => {
    let obj = JSON.parse(json);
    const foundUser = obj.users.find((user) => user.id == id);
    obj.users.splice(
      obj.users.indexOf(obj.users.find((user) => user.id === foundUser.id)),
      1
    );
    foundUser.login = req.body.login;
    foundUser.password = req.body.password;
    foundUser.role = req.body.role;
    obj.users.push(foundUser);
    fs.writeFile("./db.json", JSON.stringify(obj), function (err) {
      if (err) console.log("Error");
    });
    return res.status(200).json(obj.users);
  });
});
module.exports = router;
