const userRoutes = require("./userRoutes");
const messagesRoutes = require("./messagesRoutes");
const messageRoutes = require("./messageRoutes");
module.exports = (app) => {
  app.use("/users", userRoutes);
  app.use("/messages", messagesRoutes);
  app.use("/message", messageRoutes);
};
