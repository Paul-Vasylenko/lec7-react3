const { Router } = require("express");
const fs = require("fs");
const router = Router();

router.post("/", (req, res) => {
  fs.readFile("./db.json", (err, json) => {
    let obj = JSON.parse(json);
    obj.messages.push(req.body);
    fs.writeFile("./db.json", JSON.stringify(obj), function (err) {
      if (err) console.log("Error");
    });
    return res.status(200).json(obj.messages);
  });
});
router.put("/:id", (req, res) => {
  const { id } = req.params;
  fs.readFile("./db.json", (err, json) => {
    let obj = JSON.parse(json);
    const foundMessage = obj.messages.find((message) => message.id == id);
    obj.messages.splice(
      obj.messages.indexOf(
        obj.messages.find((message) => message.id === foundMessage.id)
      ),
      1
    );
    foundMessage.text = req.body.text;
    obj.messages.push(foundMessage);
    fs.writeFile("./db.json", JSON.stringify(obj), function (err) {
      if (err) console.log("Error");
    });
    return res.status(200).json(obj.users);
  });
});

router.delete("/:id", (req, res) => {
  const { id } = req.params;
  fs.readFile("./db.json", (err, json) => {
    let obj = JSON.parse(json);
    const foundMessage = obj.messages.find((entry) => entry.id == id);
    obj.messages.splice(
      obj.messages.indexOf(
        obj.messages.find((message) => message.id === foundMessage.id)
      ),
      1
    );
    fs.writeFile("./db.json", JSON.stringify(obj), function (err) {
      if (err) console.log("Error");
    });
    return res.status(200).json(obj.messages);
  });
});

module.exports = router;
